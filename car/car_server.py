import socket
import network as net
import threading
import time
import sys
import json
from drive_functions import CarDriver


class ClientThread(threading.Thread):

    def __init__(self, ip, port, clientsocket):

        threading.Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.clientsocket = clientsocket
        self.car = CarDriver()

    def run(self): 
   
        print("Connexion de %s %s" % (self.ip, self.port, ))
        
        while(True) :
            
            data = self.clientsocket.recv(1024)

            try:
                data = json.loads(data.decode())

                if 'move' in data and 'speed' in data:
                    if data['move'] == 'forward':
                        self.car.forward(data['speed'])
                    if data['move'] == 'backward':
                        self.car.backward(data['speed'])
                    if data['move'] == 'left':
                        self.car.left(data['speed'])
                    if data['move'] == 'right':
                        self.car.right(data['speed'])
            except json.decoder.JSONDecodeError:
                (op_code, sub_data) = net.process_packet(data)
                if op_code == net.SEND_MALUS:
                    print('RECEIVED MALUS :(')
                    self.car.stop_car()
                    time.sleep(2)

            if not data or data == b'':
                break


tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcpsock.bind(("",8888))

while True:
    tcpsock.listen(10)
    print( "Listening...")
    (clientsocket, (ip, port)) = tcpsock.accept()
    newthread = ClientThread(ip, port, clientsocket)
    newthread.start()
