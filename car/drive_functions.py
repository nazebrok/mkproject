# -*-coding:utf-8 -*
import RPi.GPIO as GPIO


LEFT_PIN = 31
RIGHT_PIN = 33
FORWARD_PIN = 35
BACKWARD_PIN = 37

freq = 60


class CarDriver:
    
    def __init__(self):
        
        GPIO.cleanup()
        GPIO.setmode(GPIO.BOARD)
        GPIO.setup(LEFT_PIN, GPIO.OUT)  # LEFT
        GPIO.setup(RIGHT_PIN, GPIO.OUT)  # RIGHT
        GPIO.setup(FORWARD_PIN ,GPIO.OUT)  # FORWARD
        GPIO.setup(BACKWARD_PIN, GPIO.OUT)  # BACKWARD

        GPIO.output(LEFT_PIN, GPIO.LOW)
        GPIO.output(RIGHT_PIN, GPIO.LOW)
        GPIO.output(FORWARD_PIN, GPIO.LOW)
        GPIO.output(BACKWARD_PIN, GPIO.LOW)
        
        self.pwm_left = GPIO.PWM(LEFT_PIN, freq)  # 50 Hz frequency
        self.pwm_right = GPIO.PWM(RIGHT_PIN, freq)  # 50 Hz frequency
        self.pwm_forward = GPIO.PWM(FORWARD_PIN, freq)  # 50 Hz frequency
        self.pwm_backward = GPIO.PWM(BACKWARD_PIN, freq)  # 50 Hz frequency

        self.pwm_left.start(0)
        self.pwm_right.start(0)
        self.pwm_forward.start(0)
        self.pwm_backward.start(0)

    def forward(self, speed):
        print("AVANT")
        self.pwm_backward.ChangeDutyCycle(0)
        self.pwm_forward.ChangeDutyCycle(speed)

    def backward(self, speed):
        print("ARRIERE")
        self.pwm_backward.ChangeDutyCycle(speed)
        self.pwm_forward.ChangeDutyCycle(0)

    def left(self, angle):
        print("LEFT")
        self.pwm_right.ChangeDutyCycle(0)
        self.pwm_left.ChangeDutyCycle(angle)
        
    def right(self, angle):
        print("RIGHT")
        self.pwm_left.ChangeDutyCycle(0)
        self.pwm_right.ChangeDutyCycle(angle)

    def release_move(self) :
        self.pwm_backward.ChangeDutyCycle(0)
        self.pwm_forward.ChangeDutyCycle(0)
    
    def release_turn(self) :
        self.pwm_right.ChangeDutyCycle(0)
        self.pwm_left.ChangeDutyCycle(0)

    def stop_car(self):
        self.pwm_left.start(0)
        self.pwm_right.start(0)
        self.pwm_forward.start(0)
        self.pwm_backward.start(0)
