import socket
import threading
import json
import signal
import re
import server.network as net
from pynput.keyboard import Key, Listener, KeyCode

central_host = "192.168.1.1"
central_port = 1337

car_host = '192.168.1.108'
car_port = 8888

socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket.connect((central_host, central_port))

speed = 100

malus_counter = 0

data = {
    'ip' : car_host,
    'port': car_port
}

socket.send(net.create_packet(net.CONNECT_TO_CAR, json.dumps(data)))


class Receiver(threading.Thread):

    def __init__(self, conn):
        threading.Thread.__init__(self)
        self.conn = conn

    def run(self):

        global malus_counter

        while(True):
            packet = self.conn.recv(1048)
            (op_code, data) = net.process_packet(packet)

            if op_code == net.GET_MALUS:
                malus_counter += 1
                print('You received a malus to use [{} left]'.format(malus_counter))

            if not data or data == b'':
                break


r = Receiver(socket)
r.start()


# Handle KILL Signal and terminate server properly
def sigterm_handler(_signo, _stack_frame):
    # Raises SystemExit(0):
    print("Close")
    socket.close()
    sys.exit(0)


signal.signal(signal.SIGTERM, sigterm_handler)


def send_message(message):
    socket.send(message)


def move_car(move):
    socket.send(net.create_packet(net.MOVE_CAR, move))


# envoie de la touche au serveur
def on_press(key):

    # print('{0} pressed'.format(key))

    if key == KeyCode.from_char('z') or key == Key.up:
        move_car(json.dumps({'move': 'forward', 'speed': speed}))

    elif key == KeyCode.from_char('q') or key == Key.left:
        move_car(json.dumps({'move': 'left', 'speed': speed}))

    elif key == KeyCode.from_char('s') or key == Key.down:
        move_car(json.dumps({'move': 'backward', 'speed': speed}))

    elif key == KeyCode.from_char('d') or key == Key.right:
        move_car(json.dumps({'move': 'right', 'speed': speed}))


# ici defintion du code p et l pour arreter les virages et l'accéleration dans certaines conditions (cf server)
def on_release(key):

    global malus_counter

    if key == KeyCode.from_char('z') or key == KeyCode.from_char('s') or key == Key.up or key == Key.down:
        move_car(json.dumps({'move': 'backward', 'speed': 0}))

    elif key == KeyCode.from_char('q') or key == KeyCode.from_char('d') or key == Key.left or key == Key.right:
        move_car(json.dumps({'move': 'left', 'speed': 0}))

    elif isinstance(key, KeyCode):
        key = str(key)[1]
        if re.match(r"\d", key):
            ip = '192.168.1.10'+key
            print("Send malus to "+ip)
            malus_counter -= 1
            if malus_counter < 0:
                malus_counter = 0
            send_message(net.create_packet(net.SEND_MALUS, ip))

    elif key == Key.esc:
        socket.close()
        return False


# Collect events until released
with Listener(on_press=on_press, on_release=on_release) as listener:
    listener.join()
