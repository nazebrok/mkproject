import socket
import threading
import signal
import sys
import network as net
import json
import random
from datetime import datetime


class ClientThread(threading.Thread):

    def __init__(self, ip, port, conn, server):

        threading.Thread.__init__(self)
        self.ip = ip
        self.port = port
        self.conn = conn
        self.server = server
        self.car = None
        self.car_ip = None
        self.print("Connected %s %s" % (self.ip, self.port))

    def wait_car_connection(self):

        while (True):
            packet = self.conn.recv(1048)
            (op_code, data) = net.process_packet(packet)
            if packet == b'':
                self.conn.close()
                self.conn = None
                return

            if op_code == net.CONNECT_TO_CAR:
                try:
                    data = json.loads(data.decode())
                    if 'ip' in data and 'port' in data:
                        if data['ip'] in self.server.player:
                            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                            s.connect((data['ip'], data['port']))
                            self.car = s
                            self.car_ip = data['ip']
                            self.server.player[data['ip']] = [self, 0]
                            self.print("Bind to car {}:{}".format(data['ip'], data['port']))
                            return
                        else:
                            self.print("Invalid Car ip {}".format(data['ip']))
                    else:
                        self.print("Wrong data Format")

                except json.decoder.JSONDecodeError as e:
                    print(e)
                    self.print("Data must be json format {'ip':ip, 'port':port}")

                except ConnectionRefusedError as e:
                    print(e)
                    self.print("No car to connect to at this address")

    def listen(self):
        
        if not self.conn:
            return

        packet = None
        while packet != ''.encode():
            try:
                packet = self.conn.recv(1048)
                (op_code, data) = net.process_packet(packet)

                if op_code == net.MOVE_CAR:
                    self.car.send(data)

                elif op_code == net.SEND_MALUS:
                    data = data.decode()
                    if self.server.player[self.car_ip][1] > 0:
                        self.server.player[self.car_ip][1] -= 1
                        self.print("Send malus to {}".format(data))
                        if data in self.server.player and self.server.player[data][0]:
                            self.server.player[data][0].car.send(net.create_packet(net.SEND_MALUS, ''))

            except socket.error as e:
                print("Client {} Disconnected".format(self.ip))

    def print(self, data):
        print("[{}] ".format(self.ip)+str(data))

    def close(self):
        if self.car_ip:
            self.server.player[self.car_ip][0] = None
            self.car.close()
        self.print("Connexion close")

    def run(self):

        self.wait_car_connection()
        self.listen()
        self.close()


class MalusGenerator(threading.Thread):

    def __init__(self, server):

        threading.Thread.__init__(self)
        self.server = server

    def generate_malus(self):
        connected_players = [p for p in self.server.player.keys() if self.server.player[p][0]]
        print(connected_players)
        if len(connected_players) == 0:
            return

        player = random.choice(connected_players)
        # Safe check if user is disconnected.
        if player in self.server.player:
            try:
                self.server.player[player][0].conn.send(net.create_packet(net.GET_MALUS, ''))
                print("{} obtained a malus".format(player))
                self.server.player[player][1] += 1
            except BrokenPipeError:
                print("[Malus Error] {} Disconnected".format(player))
            except ConnectionResetError:
                print("[Malus Error] {} Connection reset by peer".format(player))

    def run(self):
        start_time = datetime.now()

        while (True):
            time = datetime.now()
            if (time - start_time).seconds > self.server.malus_time:
                start_time = time
                self.generate_malus()


class Server:

    def __init__(self):

        print('Initialize Bullet Server ...')
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.ip = ''
        self.port = 1337
        self.malus_time = 1
        self.player = {
            '192.168.1.100': [None, 0],
            '192.168.1.101': [None, 0],
            '192.168.1.102': [None, 0],
            '192.168.1.103': [None, 0],
            '192.168.1.104': [None, 0],
            '192.168.1.105': [None, 0],
            '192.168.1.106': [None, 0],
            '192.168.1.107': [None, 0],
            '192.168.1.108': [None, 0]
        }

    def run(self):

        malus_generator = MalusGenerator(self)
        malus_generator.start()
        self.socket.bind((self.ip, self.port))
        self.socket.listen(1)
        print('Awaiting connection')

        while (True):
            conn, addr = self.socket.accept()
            client_thread = ClientThread(addr[0], addr[1], conn, self)
            client_thread.start()


if __name__ == '__main__':

    s = Server()

    # Handle KILL Signal and terminate server properly
    def sigterm_handler(_signo, _stack_frame):
        # Raises SystemExit(0):
        for car in s.player:
            if car[0]:
                car[0].conn.close()
        s.socket.close()
        sys.exit(0)


    signal.signal(signal.SIGTERM, sigterm_handler)

    try:
        # Start Server
        s.run()
    finally:
        print("Killing Server ...")
        s.socket.close()
